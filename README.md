# PADFrontEnd

npm i @fortawesome/fontawesome-free@5.13.0
npm i @fullcalendar/bootstrap@4.4.0
npm i @fullcalendar/core@4.4.0
npm i @fullcalendar/daygrid@4.4.0
npm i @fullcalendar/interaction@4.4.0
npm i @fullcalendar/timegrid@4.4.0
npm i @lgaitan/pace-progress@1.0.7
npm i @sweetalert2/theme-bootstrap-4@^3.1.4
npm i @ttskch/select2-bootstrap4-theme@^1.3.2
npm i bootstrap@^4.4.1
npm i bootstrap-colorpicker@^3.2.0
npm i bootstrap-slider@^10.6.2
npm i bootstrap-switch@3.3.4
npm i bootstrap4-duallistbox@^4.0.1
npm i bs-custom-file-input@^1.3.4
npm i chart.js@^2.9.3
npm i datatables.net@^1.10.20
npm i datatables.net-autofill-bs4@^2.3.4
npm i datatables.net-bs4@^1.10.20
npm i datatables.net-buttons-bs4@^1.6.1
npm i datatables.net-colreorder-bs4@^1.5.2
npm i datatables.net-fixedcolumns-bs4@^3.3.0
npm i datatables.net-fixedheader-bs4@^3.1.6
npm i datatables.net-keytable-bs4@^2.5.1
npm i datatables.net-responsive-bs4@^2.2.3
npm i datatables.net-rowgroup-bs4@^1.1.1
npm i datatables.net-rowreorder-bs4@^1.2.6
npm i datatables.net-scroller-bs4@^2.0.1
npm i datatables.net-select-bs4@^1.3.1
npm i daterangepicker@^3.0.5
npm i ekko-lightbox@^5.3.0
npm i fastclick@^1.0.6
npm i filterizr@^2.2.3
npm i flag-icon-css@^3.4.6
npm i flot@^4.2.0
npm i fs-extra@^9.0.0
npm i icheck-bootstrap@^3.0.1
npm i inputmask@^5.0.3
npm i ion-rangeslider@^2.3.1
npm i jquery@^3.4.1
npm i jquery-knob-chif@^1.2.13
npm i jquery-mapael@^2.2.0
npm i jquery-mousewheel@^3.1.13
npm i jquery-ui-dist@^1.12.1
npm i jquery-validation@^1.19.1
npm i jqvmap-novulnerability@^1.5.1
npm i jsgrid@^1.5.3
npm i jszip@^3.3.0
npm i moment@^2.24.0
npm i overlayscrollbars@^1.11.0
npm i pdfmake@^0.1.65
npm i popper.js@^1.16.1
npm i raphael@^2.3.0
npm i select2@^4.0.13
npm i sparklines@^1.2.0
npm i summernote@^0.8.16
npm i sweetalert2@^9.10.8
npm i tempusdominus-bootstrap-4@^5.1.2
npm i toastr@^2.1.4
=================
"scripts": {
"ng": "ng",
"start": "ng serve",
"build": "ng build",
"test": "ng test",
"lint": "ng lint",
"e2e": "ng e2e"
},
=================
npm i express path --save

en package.json pasar "typescript": "~4.0.2" en dependencies
