//Install express server
const express = require('express');
const path = require('path');

const app = express();

// Start the app by listening on the default Heroku port
app.listen(process.env.PORT || 1133);
// Serve only the static files form the angularapp directory
app.use(express.static(__dirname + '/padperu'));
app.get('/*', function(req,res) {
  res.sendFile(path.join(__dirname +'/padperu/index.html'));
});

console.log("localhost:1133");
