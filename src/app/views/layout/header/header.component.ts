import { Component, OnInit } from '@angular/core';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { Usuario } from 'src/app/models/usuario';
import { AuthService } from 'src/app/services/auth.service';
import { LoginSocialService } from '../../pages/login/loginsocial.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user: Usuario;
  constructor(
    public authService: AuthService,
    private router: Router
  ) { }
  // tslint:disable-next-line:typedef
  ngOnInit() {
    if (this.authService.isAuthenticated()){
      this.user = this.authService.usuario;
    }else {
      this.router.navigate(['login']);
    }
  }

  public logOut(): void{
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger'
      },
      buttonsStyling: true
    });

    swalWithBootstrapButtons.fire({
      title: '¿Esta seguro?',
      text: 'Va a cerrar sesión',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonText: 'Si, Cerrar Sesión',
      cancelButtonText: 'No',
      reverseButtons: false
    }).then((result) => {
      if (result.isConfirmed) {
        swalWithBootstrapButtons.fire(
          'Sesión Cerrada!',
          'Ha cerrado sesión correctamente',
          'success'
        );
        this.router.navigate(['/login']);
        this.authService.logOut();
      }
    });
  }
}
