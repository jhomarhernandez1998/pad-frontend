import { Component, OnInit } from '@angular/core';
import {SocialAuthService, SocialUser} from 'angularx-social-login';
import { Usuario } from 'src/app/models/usuario';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  user: Usuario;
  loggedIn: boolean;
  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {
    if(this.authService.isAuthenticated()) {
      this.user = this.authService.usuario;
    }
  }

}
