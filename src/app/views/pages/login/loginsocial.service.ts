import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { SocialUser , SocialAuthService, GoogleLoginProvider, FacebookLoginProvider} from 'angularx-social-login';
import Swal from 'sweetalert2';

@Injectable({
    providedIn: 'root',
})

export class LoginSocialService implements OnInit{

    user: SocialUser;
    loggedIn: boolean;

    constructor(
      private authService: SocialAuthService,
      private router: Router
    ){}

    private isNotAutorizado(e): boolean{
        if (e.status === 401 || e.status === 403){
          this.router.navigate(['/login']);
          return true;
        }
        return false;
    }


    // tslint:disable-next-line:contextual-lifecycle
    ngOnInit(){
    }

    verifySession(): SocialUser {
        this.authService.authState.subscribe((user) => {
            this.user = user;
            this.loggedIn = (user != null);
            if (this.loggedIn) {
              this.router.navigate(['teacher']);
            }else {
              this.router.navigate(['login']);
            }
        });
        return this.user;
    }

    signInWithGoogle(): void {
      this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
    }

    signInWithFB(): void {
      this.authService.signIn(FacebookLoginProvider.PROVIDER_ID);
    }

    signOut(): void {
      const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
          confirmButton: 'btn btn-success',
          cancelButton: 'btn btn-danger'
        },
        buttonsStyling: true
      });

      swalWithBootstrapButtons.fire({
        title: '¿Esta seguro?',
        text: 'Va a cerrar sesión',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, Cerrar Sesión',
        cancelButtonText: 'No',
        reverseButtons: false
      }).then((result) => {
        if (result.isConfirmed) {
          swalWithBootstrapButtons.fire(
            'Sesión Cerrada!',
            'Ha cerrado sesión correctamente',
            'success'
          );
          this.authService.signOut();
        }
      });
    }

    public getProvider(user: SocialUser): string{
      this.authService.authState.subscribe((user) => {
          return this.user = user;
      });
      return this.user.provider;
    }

    public logginInFB(): boolean{
      return JSON.stringify(this.getProvider) === 'FACEBOOK';
    }
}
