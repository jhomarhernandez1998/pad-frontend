import { Component, OnInit, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { LoginSocialService } from './loginsocial.service';
import { Usuario } from '../../../models/usuario';
import { AuthService } from '../../../services/auth.service';
import swal from 'sweetalert2';
import { i18nMetaToJSDoc } from '@angular/compiler/src/render3/view/i18n/meta';
import { TokenDto } from 'src/app/models/token-dto';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit {
  public usuario: Usuario;
  constructor(
    private router: Router,
    private socialAuthService: SocialAuthService,
    public loginSocialService: LoginSocialService,
    private authService: AuthService
  ) {
      // method to actually signin with google
      this.usuario = new Usuario();
  }

  socialUser: SocialUser;
  userLogged: SocialUser;
  loggedIn: boolean;

  ngOnInit(): void {
    //this.user = this.loginSocialService.verifySession();
    this.socialAuthService.authState.subscribe(
      data => {
          this.userLogged = data;
          this.loggedIn = this.userLogged != null;
      }
    )
    if (this.authService.isAuthenticated()){
      swal.fire('Login', `Hola ${this.authService.usuario.first_name} ya estás autenticado`, 'info');
      this.router.navigate(['teacher']);
    }
  }

  public login(): void{
    if (this.usuario.username == null || this.usuario.password == null) {
      swal.fire('Error Login', 'Email o password vacias', 'error');
      return;
    }
    this.authService.login(this.usuario).subscribe(response => {

        this.authService.guardarUsuario(response.access_token);

        this.authService.guardarToken(response.access_token);

        const usuario = this.authService.usuario;

        this.router.navigate(['teacher']);

        swal.fire('Login', `Hola ${usuario.first_name}, has iniciado sesión con éxito!`, 'success');
    }, err => {
        if (err.status === 400) {
          swal.fire('Error Login', 'Email o password incorrectas!', 'error');
        }
    });
  }

  public loginWithFB():void{
      this.authService.loginWithFB(this.usuario).then(data =>{
        this.socialUser = data;
        const tokenFace = new TokenDto(this.socialUser.authToken);
        this.authService.facebook(tokenFace).subscribe(
          res => {
            this.authService.guardarUsuario(res.value);
            this.authService.guardarToken(res.value);
            this.loggedIn = true;
            this.router.navigate(['teacher']);
            const usuario = this.authService.usuario;
            swal.fire('Login', `Hola ${usuario.first_name}, has iniciado sesión con éxito!`, 'success');
          },
          err => {
            console.log(err);
            this.authService.logOut();
          }
        );
      }).catch(
        err => {
          console.log(err);
        }
      );
  }

  public logginInGoogle():void {
     this.authService.loginWithGoogle(this.usuario).then(data =>{
        this.socialUser = data;
        const tokenGoogle = new TokenDto(this.socialUser.idToken);
        this.authService.google(tokenGoogle).subscribe(
          res => {
            this.authService.guardarUsuario(res.value);
            this.authService.guardarToken(res.value);
            this.loggedIn = true;
            this.router.navigate(['teacher']);
            const usuario = this.authService.usuario;
            swal.fire('Login', `Hola ${usuario.first_name}, has iniciado sesión con éxito!`, 'success');
          },
          err => {
            console.log(err);
            this.authService.logOut();
          }
        );
     }, err => {
        console.log("Este es el error " + err);
     });
  }
}
