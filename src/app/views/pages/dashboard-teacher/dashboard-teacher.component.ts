import { Component, OnInit } from '@angular/core';
import {TeacherService} from '../../../services/teacher.service';
import {Teacher} from '../../../models/Teacher';
import {response} from 'express';
import {Router} from '@angular/router';
import {Usuario} from '../../../models/usuario';

@Component({
  selector: 'app-dashboard-teacher',
  templateUrl: './dashboard-teacher.component.html',
  styleUrls: ['./dashboard-teacher.component.css']
})
export class DashboardTeacherComponent implements OnInit {
  // token = sessionStorage.getItem('token');
  token = null;
  teacher: Teacher[] = [];
  private profesor: Teacher = new Teacher();
  constructor(
    private router: Router,
    private teacherService: TeacherService
  ) { }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    // console.log('es token', this.token = sessionStorage.getItem('token'));
    this.token = sessionStorage.getItem('token');
    this.getAll();
  }
  getAll(): void{
    this.teacherService.getAll(this.token).subscribe(
      // tslint:disable-next-line:no-shadowed-variable
      (response) => {
        this.teacher = response;
        console.log('listar profesores', response);
      },
      error => {
        const errorMessage = error as any;
        console.error(errorMessage);
      }
    );
  }
  create(): void{
    this.teacherService.createTeacher(this.token, this.profesor).subscribe(
      // tslint:disable-next-line:no-shadowed-variable
      response => {
        console.log('crear', response);
      }
    );
  }
  update(): void {
    this.teacherService.updateTeacher(this.token, this.profesor);
  }
  // tslint:disable-next-line:typedef
  delete(teacher: Teacher): void {
    this.teacherService.deleteTeacher(this.token, teacher.id).subscribe(
      // tslint:disable-next-line:no-shadowed-variable
      response => {
        console.log('eliminando', response);
      }
    );
  }
}
