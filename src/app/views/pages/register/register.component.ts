import { Component, OnInit } from '@angular/core';
import { SocialAuthService, SocialUser } from 'angularx-social-login';
import { LoginSocialService } from '../login/loginsocial.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: SocialUser;

  constructor(
    private router: Router,
    private authService: SocialAuthService,
    public loginSocialService: LoginSocialService
  ) { }
  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.user = this.loginSocialService.verifySession();
  }
}
