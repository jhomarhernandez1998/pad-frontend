import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { SocialAuthService, SocialUser, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login';
import { TokenDto } from '../models/token-dto';
import {Usuario} from '../models/usuario';

@Injectable({
  providedIn: 'root'
})
export class AuthService {


  private headers = { headers: new HttpHeaders({'Content-Type': 'application/json'})};
  private oauthURL = 'http://localhost:8080/oauth/';
  // tslint:disable-next-line:variable-name
  private _token: string;
  // tslint:disable-next-line:variable-name
  private _usuario:Usuario;

  constructor(private http:HttpClient, private socialAuthService: SocialAuthService) { }

  public get usuario(): Usuario{
    if (this._usuario != null){
      return this._usuario;
    } else if (this._usuario == null && sessionStorage.getItem('usuario') != null){
      this._usuario = JSON.parse(sessionStorage.getItem('usuario')) as Usuario;
      return this._usuario;
    }
    return new Usuario();
  }

  public get token(): string{
    if (this._usuario != null){
      return this._token;
    } else if (this._usuario == null && sessionStorage.getItem('usuario') != null){
      this._token = sessionStorage.getItem('token');
      return this._token;
    }
    return null;
  }

  login(usuario: Usuario): Observable<any>{
    const urlEndpoint = 'http://localhost:8080/oauth/token';

    const credenciales = btoa('angularapp' + ':' + '12345');

    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/x-www-form-urlencoded',
      Authorization: 'Basic ' + credenciales
    });

    const params = new URLSearchParams();
    params.set('grant_type', 'password');
    params.set('username', usuario.username);
    params.set('password', usuario.password);
    console.log(params.toString());
    return this.http.post<any>(urlEndpoint, params.toString() , {headers: httpHeaders});
  }

  loginWithFB(usuario: Usuario): Promise<SocialUser>{
    console.log('El id es ' + FacebookLoginProvider.PROVIDER_ID);
    return this.socialAuthService.signIn(FacebookLoginProvider.PROVIDER_ID);
  }
  
  loginWithGoogle(usuario: Usuario):Promise<SocialUser>{
    return this.socialAuthService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  public google(tokenDto:TokenDto): Observable<TokenDto>{
    return this.http.post<TokenDto>(this.oauthURL + 'google', tokenDto, this.headers);
  }

  public facebook(tokenDto:TokenDto): Observable<TokenDto>{
    return this.http.post<TokenDto>(this.oauthURL + 'facebook', tokenDto, this.headers);
  }

  // tslint:disable-next-line:variable-name
  guardarUsuario(access_token: string): void{
    const payload = this.obtenerDatosToken(access_token);
    console.log(payload);
    this._usuario = new Usuario();
    this._usuario.first_name = payload.nombre;
    this._usuario.last_name = payload.apellido;
    this._usuario.username = payload.email;
    this._usuario.picture = payload.picture;
    this._usuario.roles = payload.authorities;
    sessionStorage.setItem('usuario', JSON.stringify(this._usuario));
  }

  guardarUsuarioSocial(response: any): void{
    this._usuario = new Usuario();
    this._usuario.first_name = response.firstName;
    this._usuario.last_name = response.lastName;
    this._usuario.username = response.email;
  }

  // tslint:disable-next-line:variable-name
  guardarToken(access_token: string): void{
    this._token = access_token;
    sessionStorage.setItem('token', this._token);
  }

  // tslint:disable-next-line:variable-name
  obtenerDatosToken(access_token: string): any{
    if (access_token != null){
      return JSON.parse(atob(access_token.split('.')[1]));
    }
    return null;
  }

  isAuthenticated(): boolean{
    const payload = this.obtenerDatosToken(this.token);
    if (payload != null && payload.nombre && payload.nombre.length > 0){
      return true;
    }
    return false;
  }

  logOut(): void{
    this._token = null;
    this._usuario = null;
    sessionStorage.removeItem('usuario');
    sessionStorage.removeItem('token');
  }
}
