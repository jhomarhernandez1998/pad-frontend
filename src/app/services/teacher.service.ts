import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {Teacher} from '../models/Teacher';
import swal from 'sweetalert2';
import {catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class TeacherService {
  private url = environment.BASEURL + '/teacher';
  constructor(
    private http: HttpClient
  ) { }
  /************************************************/
  // tslint:disable-next-line:variable-name
  getAll(auth_token: string): Observable<Teacher[]>{
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${auth_token}`
    });
    // @ts-ignore
    return this.http.get<Teacher[]>(this.url + '/lister', {headers : httpHeaders}).pipe(
      catchError(e => {
        console.error(e.error.message);
        // swal('Error al editar', e.error.mensaje, 'error');
        return throwError(e);
      })
    );
  }
  // tslint:disable-next-line:variable-name
  createTeacher(auth_token: string, teacher: Teacher): Observable<Teacher> {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${auth_token}`
    });
    // @ts-ignore
    return this.http.post<Teacher>(this.url + '/create', teacher, {headers : httpHeaders});
  }
  // tslint:disable-next-line:variable-name
  updateTeacher(auth_token: string, teacher: Teacher): Observable<Teacher> {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${auth_token}`
    });
    // @ts-ignore
    return this.http.put<Teacher>(this.url + `/update/${teacher.id}`, teacher, {headers : httpHeaders});
  }
  // tslint:disable-next-line:variable-name
  deleteTeacher(auth_token: string, id: number): Observable<Teacher> {
    const httpHeaders = new HttpHeaders({
      'Content-Type': 'application/json',
      Authorization: `Bearer ${auth_token}`
    });
    // @ts-ignore
    return this.http.delete<Teacher>(this.url + `/delete/${id}`, {headers : httpHeaders});
  }
  /************************************************/
}
