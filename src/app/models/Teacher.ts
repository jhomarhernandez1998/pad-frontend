export class Teacher {
  id: number;
  identification: string;
  names: string;
  // tslint:disable-next-line:variable-name
  first_name: string;
  // tslint:disable-next-line:variable-name
  last_name: string;
  // tslint:disable-next-line:variable-name
  fec_nac: string;
  sexo: string;
  // tslint:disable-next-line:variable-name
  civil_status: string;
  direction: string;
  tlf: string;
  cel: string;
  // tslint:disable-next-line:variable-name
  cel_emergency: string;
}
