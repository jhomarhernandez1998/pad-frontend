export class Usuario {
  id: number;
  username: string;
  // tslint:disable-next-line:variable-name
  facebook_id: string;
  // tslint:disable-next-line:variable-name
  google_id: string;
  password: string;
  picture: string;
  // tslint:disable-next-line:variable-name
  first_name: string;
  // tslint:disable-next-line:variable-name
  last_name: string;
  deleted: boolean;
  roles: string[] = [];
}
