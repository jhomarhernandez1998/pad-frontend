import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { SocialLoginModule, SocialAuthServiceConfig, GoogleLoginProvider, FacebookLoginProvider } from 'angularx-social-login'

import { LoginSocialService } from './views/pages/login/loginsocial.service';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './views/pages/login/login.component';
import { RegisterComponent } from './views/pages/register/register.component';
import {HeaderComponent} from './views/layout/header/header.component';
import {SidebarComponent} from './views/layout/sidebar/sidebar.component';
import {FooterComponent} from './views/layout/footer/footer.component';
import {DashboardTeacherComponent} from './views/pages/dashboard-teacher/dashboard-teacher.component';
import {DashboardStudentComponent} from './views/pages/dashboard-student/dashboard-student.component';
import {MuroComponent} from './views/pages/muro/muro.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    FooterComponent,
    LoginComponent,
    RegisterComponent,
    DashboardTeacherComponent,
    DashboardStudentComponent,
    MuroComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    SocialLoginModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: 'SocialAuthServiceConfig',
      useValue: {
        autoLogin: false,
        providers : [
          {
            id: GoogleLoginProvider.PROVIDER_ID,
            provider: new GoogleLoginProvider('288628588407-c85ha7sk49lslreubi4fl8p88dhev720.apps.googleusercontent.com')
          },
          {
            id: FacebookLoginProvider.PROVIDER_ID,
            provider: new FacebookLoginProvider('247652660263935')
          }
        ]
      } as SocialAuthServiceConfig,
    },
    LoginSocialService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
