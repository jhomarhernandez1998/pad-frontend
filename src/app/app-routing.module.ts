import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from './views/pages/login/login.component';
import { RegisterComponent } from './views/pages/register/register.component';
import {DashboardTeacherComponent} from './views/pages/dashboard-teacher/dashboard-teacher.component';
import {DashboardStudentComponent} from './views/pages/dashboard-student/dashboard-student.component';
import {MuroComponent} from './views/pages/muro/muro.component';

const routes: Routes = [
  {path: '', redirectTo: '/login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'teacher', component: DashboardTeacherComponent},
  {path: 'student', component: DashboardStudentComponent},
  {path: 'muro', component: MuroComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
